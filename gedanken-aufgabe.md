# Aufgabe

Du hast vor dir 10 Maschinen die Münzen prägen. Die Münzen sehen alle gleich aus! Die Münzen wiegen 100 Gramm.

Allerdings ist eine Maschine kaputt und die Münzen der Maschine wiegen 10 Gramm.

Münzen stehen unendlich pro Maschine bereit und es gibt eine Digitalwaage mit der ich einmal wiegen darf,
um heraus zu finden welche Maschine defekt ist. Welche Maschine ist defekt?

# Gedanken

- da ich nur einmal wiegen darf, kann ich nicht mit einer Wahrscheinlichkeitsrechnung arbeiten, um mich der Lösung zu nähern
- ich muss mit einmal wiegen mathematisch bestimmen können, aus welche Maschine das falsche Gewicht kommt
- 10g ist ein 1/10 von 100g
- erwartete Ausgabe bei 10 Münzen je Maschine; = 100 Münzen
    - expected: 100 * 100g = 10.000g
    - actual:   90 * 100g + 10 * 10g = 9.100g
- wenn jede Maschine die gleiche Anzahl Münzen erzeugt, kann ich nicht sagen, aus welcher die defekte Menge kommt
    - weil die defekte Menge immer gleich groß ist
- wenn aber jede Maschine eine andere Menge von Münzen generiert, ergibt sich ein je Maschine eindeutiges Ergebnis

## Lösungsansatz

MA = Maschine
mü = Münze

MA 1 : 1mü
MA 2 : 2mü
MA 3 : 3mü
MA 4 : 4mü
MA 5 : 5mü
MA 6 : 6mü
MA 7 : 7mü
MA 8 : 8mü
MA 9 : 9mü
MA 10: 10mü
= 55mü

expected: 55mü * 100g = 5.500g
actual:

- MA 1 ist defekt: 10g * 1mü + 54mü * 100g = 5410
- MA 2 ist defekt: 10g * 2mü + 53mü * 100g = 5320
- MA 3 ist defekt: 10g * 3mü + 52mü * 100g = 5230
- MA 4 ist defekt: 10g * 4mü + 51mü * 100g = 5140
- MA 5 ist defekt: 10g * 5mü + 50mü * 100g = 5050
- MA 5 ist defekt: 10g * 6mü + 49mü * 100g = 4960
- MA 5 ist defekt: 10g * 7mü + 48mü * 100g = 4870
- MA 5 ist defekt: 10g * 8mü + 47mü * 100g = 4780
- MA 5 ist defekt: 10g * 9mü + 46mü * 100g = 4690
- MA 5 ist defekt: 10g * 10mü + 45mü * 100g = 4600

# Lösung

Ich entnehme aus jeder Maschine je eine weitere Münze, angefangen bei Maschine 1 mit einer Münze.\
Aus Maschine 2 genau zwei Münzen, aus Maschine 3 drei Münzen bis zu Maschine 10 zehn Münzen.

Wiege ich diese 55 Münzen, erhalte ich ein unterschiedliches Ergebnis, je nachdem welche Maschine defekt ist.
Somit weiß ich, welche Maschine defekt ist, indem ich die möglichen Ergebnisse auswerte.

(siehe Liste im Lösungsansatz)
