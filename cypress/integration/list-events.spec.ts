import emptyEventsFixture from 'cypress/fixtures/empty-events.json';
import eventsWithoutTicketsFixture from 'cypress/fixtures/events-without-tickets.json';
import eventsWitTicketsFixture from 'cypress/fixtures/events-with-tickets.json';
import { TestId } from 'src/utils/TestId';

const mockEvents = (body: object) => cy.intercept('GET', '/api/events*', {
    statusCode: 200,
    body,
});

describe('List events', () => {
    it('shows a information if no events are present', () => {
        mockEvents(emptyEventsFixture);

        cy.visit('/');
        cy.contains('No events defined yet.');
    });

    it('lists events', () => {
        mockEvents(eventsWithoutTicketsFixture);

        cy.visit('/');
        cy.get(`[data-test-id="${TestId.eventListTableRoot}"] > tr`)
            .should('have.length', 4);
    });

    it('disable ticket table', () => {
        mockEvents(eventsWithoutTicketsFixture);

        cy.visit('/');
        cy.get(`[data-test-id="${TestId.eventListTicketToggleTableCell}"] > span`)
            .should('have.attr', 'aria-label', 'No tickets for this event');
    });

    it('shows ticket table', () => {
        mockEvents(eventsWitTicketsFixture);

        cy.visit('/');

        cy.get(`[data-test-id="${TestId.eventListTicketToggleTableCell}"] > button`)
            .should('have.attr', 'aria-label', 'Show tickets')
            .click();

        cy.get(`[data-test-id="${TestId.ticketTableRow}"]`)
            .should('have.length', 3);
    });
});
