import { TestId } from 'src/utils/TestId';
import { Event } from 'src/types/Event';
import { EventTicket } from 'src/types/EventTicket';

const mockEvents = (body: object, statusCode = 200) => cy.intercept('POST', '/api/events*', {
    statusCode,
    body,
});

describe('Create event', () => {
    it('prevents creating empty or invalid events', () => {
        mockEvents({});

        cy.visit('/create-event');

        // Test empty
        cy.get(`[data-test-id="${TestId.createEventFormSubmit}"]`)
            .click();

        cy.contains('eventTitle must be at least 5 characters');
        cy.contains('eventCity must be at least 1 characters');

        // Test out of range
        cy.get('#eventTitle').type('A'.repeat(257));
        cy.get('#eventCity').type('A'.repeat(257));

        cy.get(`[data-test-id="${TestId.createEventFormSubmit}"]`)
            .click();

        cy.contains('eventTitle must be at most 256 characters');
        cy.contains('eventCity must be at most 256 characters');
    });

    it('prevents creating empty or invalid tickets', () => {
        mockEvents({});

        cy.visit('/create-event');

        // Test empty
        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        cy.contains('firstName must be at least 1 characters');
        cy.contains('lastName must be at least 1 characters');
        cy.contains('barcode must be at least 1 characters');

        // Test out of range
        cy.get('#firstName').type('A'.repeat(257));
        cy.get('#lastName').type('A'.repeat(257));
        cy.get('#barcode').type('A'.repeat(9));

        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        cy.contains('firstName must be at most 256 characters');
        cy.contains('lastName must be at most 256 characters');
        cy.contains('barcode must be at most 8 characters');
    });

    it('shows an error notification on error', () => {
        mockEvents({ acknowledged: false }, 500);

        cy.visit('/create-event');

        cy.get('#eventTitle').type('Test Title');
        cy.get('#eventCity').type('Test City');

        cy.get(`[data-test-id="${TestId.createEventFormSubmit}"]`)
            .click();

        cy.contains('#notistack-snackbar', 'Event creation failed');
    });

    it('allows creating events without tickets', () => {
        cy.intercept('POST', '/api/events*', req => {
            const jsonBody: { event: Event, tickets: EventTicket[] } = JSON.parse(req.body);
            expect(jsonBody.event.eventTitle).to.eq('Test Title');
            expect(jsonBody.event.eventCity).to.eq('Test City');
            // YYYY-MM-DDTHH:mm:ss.sssZ
            expect(jsonBody.event.eventDate).to.contains(new Date().toISOString().substring(0, 10));
            expect(jsonBody.tickets).to.have.length(0);
        });

        cy.visit('/create-event');

        cy.get('#eventTitle').type('Test Title');
        cy.get('#eventCity').type('Test City');

        cy.get(`[data-test-id="${TestId.createEventFormSubmit}"]`)
            .click();

        cy.contains('#notistack-snackbar', 'Event created');
    });

    it('allows creating events with tickets', () => {
        cy.intercept('POST', '/api/events*', req => {
            const jsonBody: { event: Event, tickets: EventTicket[] } = JSON.parse(req.body);
            expect(jsonBody.event.eventTitle).to.eq('Test Title');
            expect(jsonBody.event.eventCity).to.eq('Test City');
            // YYYY-MM-DDTHH:mm:ss.sssZ
            expect(jsonBody.event.eventDate).to.contains(new Date().toISOString().substring(0, 10));
            expect(jsonBody.tickets).to.have.length(3);
            expect(jsonBody.tickets[0].barcode).to.eq('1'.repeat(8));
            expect(jsonBody.tickets[2].barcode).to.eq('3'.repeat(8));
        });

        cy.visit('/create-event');

        cy.get('#eventTitle').type('Test Title');
        cy.get('#eventCity').type('Test City');

        cy.get('#firstName').type('Test');
        cy.get('#lastName').type('Name');
        cy.get('#barcode').type('1'.repeat(8));

        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        cy.get('#firstName').type('Test 2');
        cy.get('#lastName').type('Name 2');
        cy.get('#barcode').type('2'.repeat(8));

        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        cy.get('#firstName').type('Test 3');
        cy.get('#lastName').type('Name 3');
        cy.get('#barcode').type('3'.repeat(8));

        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        cy.get('#firstName').type('Test 4');
        cy.get('#lastName').type('Name 4');
        cy.get('#barcode').type('4'.repeat(8));

        cy.get(`[data-test-id="${TestId.createTicketFormSubmit}"]`)
            .click();

        // Delete the last entry
        cy.get(`[data-test-id="${TestId.deleteTicketButton}"]`)
            .eq(3)
            .click();

        cy.contains('#notistack-snackbar', 'Ticket removed');

        cy.get(`[data-test-id="${TestId.createEventFormSubmit}"]`)
            .click();

        cy.contains('#notistack-snackbar', 'Event created');
    });

});
