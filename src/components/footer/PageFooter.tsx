import React, { ReactElement } from 'react';
import LoveHeart from 'src/components/footer/LoveHeart';
import { alpha, styled } from '@mui/system';

// Required to offset down the previous content
const FooterOffset = styled('div')(({ theme }) => ({
    marginTop: '10rem',
}));

const FooterContainer = styled('footer')(({ theme }) => ({
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,

    display: 'flex',
    flex: 1,
    padding: '2rem 0',
    borderTop: `1px solid ${alpha(theme.palette.primary.light, 0.2)}`,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.palette.common.black
}));

const PageFooter = (): ReactElement => (
    <>
        <FooterOffset />

        <FooterContainer>
            Made with <LoveHeart /> in NRW
        </FooterContainer>
    </>
);

export default PageFooter;
