import React, { ReactElement } from 'react';
import { Favorite } from '@mui/icons-material';
import { keyframes, styled } from '@mui/material';

const heartBeatPulse = keyframes`
  0% {
    transform: scale(0.8);
    opacity: 1;
  }
  50% {
    transform: scale(1.3);
    opacity: 0.3;
  }
  100% {
    transform: scale(0.8);
    opacity: 1;
  }
`;

const beatsPerSecond = (60 / 80).toFixed(1);

const HeartContainer = styled('div')({
    padding: '0 0.5rem',
    color: '#ff0000',
    animation: `${heartBeatPulse} ${beatsPerSecond}s infinite ease`,
});

const LoveHeart = (): ReactElement => (
    <HeartContainer>
        <Favorite />
    </HeartContainer>
);

export default LoveHeart;
