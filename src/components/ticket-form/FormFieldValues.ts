import * as yup from 'yup';
import { validationSchema } from 'src/components/ticket-form/validationSchema';

export type FormFieldValues = yup.InferType<typeof validationSchema>;
export type FieldName = keyof FormFieldValues;
