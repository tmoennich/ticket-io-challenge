import { Button, Grid } from '@mui/material';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { ReactElement, useCallback } from 'react';
import ManagedTextField from 'src/components/fields/ManagedTextField';
import { FieldName, FormFieldValues } from 'src/components/ticket-form/FormFieldValues';
import { validationSchema } from 'src/components/ticket-form/validationSchema';
import { Add } from '@mui/icons-material';
import { EventTicket } from 'src/types/EventTicket';
import { TestId } from 'src/utils/TestId';

type Props = {
    defaultValues?: Partial<FormFieldValues>;
    onCreate: (ticket: EventTicket) => void,
}

const TicketForm = ({
    defaultValues,
    onCreate,
}: Props): ReactElement => {

    const {
        handleSubmit,
        reset,
        control,
        clearErrors
    } = useForm<FormFieldValues>({
        defaultValues,
        mode: 'onBlur',
        resolver: yupResolver(validationSchema),
        shouldFocusError: true,
        criteriaMode: 'all',
        reValidateMode: 'onChange',
    });

    const handleFormSubmit = useCallback((data: FormFieldValues) => {
        reset();

        onCreate(data);
    }, [reset, onCreate]);

    const TextField = ({ name, label }: { name: FieldName, label: string }): ReactElement => (
        <ManagedTextField<FormFieldValues>
            label={label}
            name={name}
            defaultValue={defaultValues?.[name]}
            clearErrors={clearErrors}
            control={control}
        />
    );

    return (
        <form onSubmit={handleSubmit(handleFormSubmit)}>
            <Grid container spacing={4}>
                <Grid item xs={6}>
                    <TextField
                        label="First name"
                        name="firstName"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        label="Last name"
                        name="lastName"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        label="Barcode"
                        name="barcode"
                    />
                </Grid>

                <Grid item xs={12}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        startIcon={<Add />}
                        data-test-id={TestId.createTicketFormSubmit}
                    >
                        Add
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default TicketForm;
