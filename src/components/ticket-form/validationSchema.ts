import * as yup from 'yup';

export const validationSchema = yup.object({
    firstName: yup.string().min(1).max(256).required(),
    lastName: yup.string().min(1).max(256).required(),
    barcode: yup.string().min(1).max(8).required(),
}).required();
