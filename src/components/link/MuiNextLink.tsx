// Inspired from https://github.com/mui/material-ui/blob/master/examples/nextjs/src/Link.js
// Copied https://stackoverflow.com/a/67469832

import React, { forwardRef, Ref } from 'react';
import Link, { LinkProps } from 'next/link';
import { Link as MuiLink, LinkProps as MuiLinkProps, styled } from '@mui/material';

const StyledMuiLink = styled(MuiLink)(({ theme }) => ({
    color: theme.palette.common.white,
    textDecoration: 'none',
    borderBottom: '1px solid transparent',
    ':hover': {
        borderColor: theme.palette.common.white,
    }
}));

type LinkRef = HTMLAnchorElement;
type NextLinkProps = Omit<MuiLinkProps, 'href' | 'classes'> &
    Pick<LinkProps, 'href' | 'as' | 'prefetch'>;

const MuiNextLink = ({ href, as, prefetch, ...props }: LinkProps, ref: Ref<LinkRef>) => (
    <Link
        href={href}
        as={as}
        prefetch={prefetch}
        passHref={true}
    >
        <StyledMuiLink
            ref={ref}
            {...props}
        />
    </Link>
);

export default forwardRef<LinkRef, NextLinkProps>(MuiNextLink);
