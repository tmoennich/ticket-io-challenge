import { Control, Controller, FieldPath, UseFormClearErrors } from 'react-hook-form';
import { TextField } from '@mui/material';
import { ReactElement, useCallback } from 'react';
import { FieldValues } from 'react-hook-form/dist/types';

type Props<T extends FieldValues> = {
    name: FieldPath<T>;
    label: string;
    defaultValue?: any;
    clearErrors: UseFormClearErrors<T>;
    control: Control<T>;
}

const ManagedTextField = <T extends FieldValues>({
    name,
    label,
    defaultValue,
    clearErrors,
    control,
}: Props<T>): ReactElement => {

    const handleInputFocus = useCallback(() => {
        clearErrors(name);
    }, [clearErrors, name]);

    return (
        <Controller
            name={name}
            control={control}
            // Required to set it anyway to prevent switching from uncontrolled to controlled input
            defaultValue={defaultValue ?? ''}
            render={({ field: { onChange, onBlur, value }, fieldState: { error } }) => (
                <TextField
                    fullWidth={true}
                    id={name}
                    name={name}
                    label={label}
                    autoComplete={name}
                    value={value}
                    onChange={onChange}
                    onBlur={onBlur}
                    onFocus={handleInputFocus}
                    error={error !== undefined}
                    helperText={error ? error.message : null}
                    variant="standard"
                />
            )}
        />
    );
};

export default ManagedTextField;