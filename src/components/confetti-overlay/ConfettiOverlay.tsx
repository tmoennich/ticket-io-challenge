import React, { ReactElement, useRef, useState } from 'react';
import { useWindowSize } from 'react-use';
import ReactConfetti from 'react-confetti';
import Confetti from 'react-confetti/dist/types/Confetti';

export const useConfettiOverlay = (delay = 6000): [boolean, () => void, (confetti?: Confetti) => void] => {
    const [isActive, setIsActive] = useState(false);
    const timerRef = useRef<number | null>(null);

    const showConfetti = (show = true) => {
        setIsActive(show);

        if (show) {
            if (timerRef.current !== null) {
                window.clearTimeout(timerRef.current);
            }

            timerRef.current = window.setTimeout(() => onConfettiComplete(), delay);
        }
    };

    const onConfettiComplete = (confetti?: Confetti) => {
        if (timerRef.current !== null) {
            window.clearTimeout(timerRef.current);
        }

        confetti?.reset();
        setIsActive(false);
    };

    return [isActive, showConfetti, onConfettiComplete];
};

interface Props {
    onComplete: (confetti?: Confetti) => void;
}

const ConfettiOverlay = ({ onComplete }: Props): ReactElement => {

    const { width, height } = useWindowSize();

    return (
        <ReactConfetti
            width={width}
            height={height}
            onConfettiComplete={onComplete}
            recycle={false}
            numberOfPieces={100}
            confettiSource={{
                w: 10,
                h: 10,
                x: width / 2,
                y: height / 2,
            }}
        />
    );
};

export default ConfettiOverlay;
