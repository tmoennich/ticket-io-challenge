import * as yup from 'yup';
import { validationSchema } from 'src/components/event-form/validationSchema';

export type FormFieldValues = yup.InferType<typeof validationSchema>;
export type TextFieldName = Exclude<keyof FormFieldValues, 'eventDate'>;

