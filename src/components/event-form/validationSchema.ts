import * as yup from 'yup';

export const validationSchema = yup.object({
    eventTitle: yup.string().min(5).max(256).required(),
    // https://www.google.com/search?q=shortest+city+name
    // => A single letter
    eventCity: yup.string().min(1).max(256).required(),
    eventDate: yup.date().required(),
}).required();
