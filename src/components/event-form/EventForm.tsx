import { Box, Button, Grid, Paper, TextField as MuiTextField, Typography } from '@mui/material';
import { DesktopDatePicker, LoadingButton } from '@mui/lab';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { ReactElement, SyntheticEvent, useCallback, useState } from 'react';
import ManagedTextField from 'src/components/fields/ManagedTextField';
import { FormFieldValues, TextFieldName } from 'src/components/event-form/FormFieldValues';
import { validationSchema } from 'src/components/event-form/validationSchema';
import TicketForm from 'src/components/ticket-form/TicketForm';
import DenseTicketList from 'src/components/dense-ticket-list/DenseTicketList';
import useTicketList from 'src/hooks/useTicketList';
import { EventTicket } from 'src/types/EventTicket';
import { Event } from 'src/types/Event';
import { Save } from '@mui/icons-material';
import ConfettiOverlay, { useConfettiOverlay } from 'src/components/confetti-overlay/ConfettiOverlay';
import { TestId } from 'src/utils/TestId';

type Props = {
    defaultValues?: FormFieldValues;
    onCreate?: (data: Event, tickets: EventTicket[]) => Promise<boolean>;
}

const EventForm = ({
    defaultValues,
    onCreate,
}: Props): ReactElement => {

    const [ticketList, addTicket, removeTicket, resetTickets] = useTicketList([]);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [isConfettiActive, showConfetti, onConfettiComplete] = useConfettiOverlay();

    const {
        handleSubmit,
        reset,
        control,
        clearErrors,
    } = useForm<FormFieldValues>({
        defaultValues,
        mode: 'onBlur',
        resolver: yupResolver(validationSchema),
        shouldFocusError: true,
        criteriaMode: 'all',
        reValidateMode: 'onChange',
    });

    const handleFormSubmit = async (data: FormFieldValues) => {
        setIsSubmitting(true);

        if (!onCreate) {
            return;
        }

        const creationResult = await onCreate(data, ticketList);
        if (!creationResult) {
            setIsSubmitting(false);

            return;
        }

        reset();
        resetTickets();

        setIsSubmitting(false);
        showConfetti();
    };

    const submitHandlerFunction = handleSubmit(handleFormSubmit);

    const handleSubmitButtonClick = useCallback((event: SyntheticEvent) => {
        event.preventDefault();
        event.stopPropagation();

        submitHandlerFunction();
    }, [submitHandlerFunction]);

    const TextField = ({ name, label }: { name: TextFieldName, label: string }): ReactElement => (
        <ManagedTextField<FormFieldValues>
            label={label}
            name={name}
            defaultValue={defaultValues?.[name]}
            clearErrors={clearErrors}
            control={control}
        />
    );

    return (
        <>
            <form onSubmit={submitHandlerFunction} data-test-id={TestId.createEventForm}>
                <Grid container spacing={4}>
                    <Grid item xs={12}>
                        <TextField
                            label="Title"
                            name="eventTitle"
                        />
                    </Grid>
                    <Grid item xs={8}>
                        <TextField
                            label="City"
                            name="eventCity"
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Controller
                            control={control}
                            name="eventDate"
                            defaultValue={defaultValues?.eventDate ?? new Date()}
                            render={({ field: { onChange, onBlur, value } }) => (
                                <DesktopDatePicker
                                    value={value}
                                    label="Date desktop"
                                    inputFormat="dd.MM.yyyy"
                                    mask="__.__.____"
                                    onChange={onChange}
                                    onClose={onBlur}
                                    renderInput={(params) => <MuiTextField {...params} />}
                                />
                            )}
                        />
                    </Grid>
                </Grid>
            </form>

            <Grid container spacing={4}>
                <Grid item xs={12}>
                    <Paper variant="outlined" sx={{ my: 6, p: 3 }}>
                        {ticketList.length > 0 && (
                            <Box sx={{ mb: 6 }}>
                                <Typography component="h2" variant="h4" align="center" sx={{ mb: 2 }}>
                                    Tickets
                                </Typography>

                                <DenseTicketList
                                    ticketList={ticketList}
                                    allowDeletion={true}
                                    onDelete={removeTicket}
                                />
                            </Box>
                        )}

                        <Typography component="h2" variant="h4" align="center" sx={{ mb: 2 }}>
                            Add a new ticket
                        </Typography>

                        <TicketForm onCreate={addTicket} />
                    </Paper>
                </Grid>

                <Grid item xs={12}>
                    {isSubmitting ? (
                        <LoadingButton
                            loading={true}
                            loadingPosition="start"
                            startIcon={<Save />}
                            variant="contained"
                            color="success"
                            sx={{ width: '100%' }}
                        >
                            Creating event
                        </LoadingButton>
                    ) : (
                        <Button
                            variant="contained"
                            color="success"
                            sx={{ width: '100%' }}
                            onClick={handleSubmitButtonClick}
                            data-test-id={TestId.createEventFormSubmit}
                        >
                            Create event
                        </Button>
                    )}
                </Grid>
            </Grid>

            {isConfettiActive && (
                <ConfettiOverlay onComplete={onConfettiComplete} />
            )}
        </>
    );
};

export default EventForm;
