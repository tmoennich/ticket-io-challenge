import React, { ReactElement, useCallback } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { EventTicket } from 'src/types/EventTicket';
import { IconButton } from '@mui/material';
import { Delete } from '@mui/icons-material';
import { TestId } from 'src/utils/TestId';

const barcodeColumnWidth = 120;
const actionColumnWidth = 40;

type Props = {
    ticketList: EventTicket[];
    allowDeletion?: boolean;
    onDelete?: (index: number) => void;
}

const DenseTicketList = ({
    ticketList,
    allowDeletion = false,
    onDelete,
}: Props): ReactElement => {

    const createDeleteHandler = useCallback((index: number) => {
        return () => onDelete?.(index);
    }, [onDelete]);

    return (
        <TableContainer component={Paper}>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell width={barcodeColumnWidth}>Barcode</TableCell>
                        {allowDeletion && (
                            <TableCell width={actionColumnWidth} />
                        )}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {ticketList.map(({ firstName, lastName, barcode }, index) => (
                        <TableRow
                            key={barcode}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {[firstName, lastName].join(' ')}
                            </TableCell>
                            <TableCell width={barcodeColumnWidth}>{barcode}</TableCell>
                            {allowDeletion && (
                                <TableCell width={actionColumnWidth}>
                                    <IconButton
                                        color="primary"
                                        component="span"
                                        onClick={createDeleteHandler(index)}
                                        data-test-id={TestId.deleteTicketButton}
                                    >
                                        <Delete />
                                    </IconButton>
                                </TableCell>
                            )}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default DenseTicketList;
