import { ReactElement } from 'react';
import { AppBar, Container, styled, Toolbar } from '@mui/material';
import HomeIcon from 'src/components/header/HomeIcon';
import MuiNextLink from 'src/components/link/MuiNextLink';
import NavBar from 'src/components/header/NavBar';
import navLinks from 'src/configuration/navLinks';

const StyledAppBar = styled(AppBar)({
    position: 'fixed',
});

// Required to offset down the following content after the fixed AppBar
const HeaderOffset = styled('div')(({ theme }) => ({
    ...theme.mixins.toolbar,
}));

const ToolbarContainer = styled(Container)({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
});

const PageHeader = (): ReactElement => (
    <>
        <StyledAppBar>
            <Toolbar>
                <ToolbarContainer>
                    <MuiNextLink href="/">
                        <HomeIcon />
                    </MuiNextLink>

                    <NavBar navLinks={navLinks} />
                </ToolbarContainer>
            </Toolbar>
        </StyledAppBar>

        <HeaderOffset />
    </>
);

export default PageHeader;
