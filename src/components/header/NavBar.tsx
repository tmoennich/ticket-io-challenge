import Stack from '@mui/material/Stack';
import MuiNextLink from 'src/components/link/MuiNextLink';
import { ReactElement } from 'react';
import { NavLink } from 'src/configuration/navLinks';
import { styled } from '@mui/material';

const StyledLink = styled(MuiNextLink)(({ theme }) => ({
    color: theme.palette.common.white,
    opacity: 0.8,
    display: 'inline-flex',
    alignItems: 'stretch',
}));

const StyledToolbar = styled('nav')(({ theme }) => ({
    ...theme.mixins.toolbar,
    display: 'flex',
    alignItems: 'center',
}));

type Props = {
    navLinks: NavLink[];
}

const Navbar = ({ navLinks }: Props): ReactElement => (
    <StyledToolbar>
        <Stack direction="row" spacing={4}>
            {navLinks.map(({ title, path }, i) => (
                <StyledLink
                    key={`${title}${i}`}
                    href={path}
                    variant="button"
                >
                    {title}
                </StyledLink>
            ))}
        </Stack>
    </StyledToolbar>
);

export default Navbar;
