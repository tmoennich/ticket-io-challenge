import React, { ReactElement } from 'react';
import { IconButton, styled } from '@mui/material';
import { Home } from '@mui/icons-material';

const StyledHomeIcon = styled(Home)(({ theme }) => ({
    color: theme.palette.common.white,
}));

const HomeIcon = (): ReactElement => (
    <IconButton>
        <StyledHomeIcon fontSize="large" />
    </IconButton>
);

export default HomeIcon;
