import {
    Box,
    Collapse,
    IconButton,
    Paper,
    styled,
    Table,
    TableBody,
    TableCell,
    tableCellClasses,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import React, { ReactElement } from 'react';
import { Event } from 'src/types/Event';
import { KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material';
import { format } from 'date-fns';
import { useToggle } from 'react-use';
import { TestId } from 'src/utils/TestId';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const TableRowWithTickets = (props: { row: Event }) => {
    const { row } = props;
    const [showTickets, toggleTickets] = useToggle(false);

    const eventTickets = row.tickets ?? [];

    return (
        <>
            <StyledTableRow>
                <StyledTableCell data-test-id={TestId.eventListTicketToggleTableCell}>
                    {eventTickets.length > 0 ? (
                        <Tooltip title={showTickets ? 'Hide tickets' : 'Show tickets'} arrow={true}>
                            <IconButton
                                size="small"
                                onClick={toggleTickets}
                            >
                                {showTickets ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                            </IconButton>
                        </Tooltip>
                    ) : (
                        <Tooltip title="No tickets for this event" arrow={true}>
                            {/* span is required to trigger the tooltip even if it's disabled */}
                            <span>
                                <IconButton size="small" disabled={true}>
                                    <KeyboardArrowDown />
                                </IconButton>
                            </span>
                        </Tooltip>
                    )}
                </StyledTableCell>
                <StyledTableCell component="th" scope="row">
                    {row.eventTitle}
                </StyledTableCell>
                <StyledTableCell align="right">{row.eventCity}</StyledTableCell>
                <StyledTableCell align="right">{format(row.eventDate, 'dd.MM.yyyy')}</StyledTableCell>
            </StyledTableRow>

            {eventTickets.length > 0 && (
                <StyledTableRow>
                    <StyledTableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={4}>
                        <Collapse in={showTickets} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                                <Typography variant="h6" gutterBottom component="div">
                                    Tickets
                                </Typography>
                                <Table size="small" aria-label="purchases">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell>Name</StyledTableCell>
                                            <StyledTableCell align="right">Barcode</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {eventTickets.map(eventTicket => (
                                            <StyledTableRow
                                                key={[eventTicket.barcode, eventTicket.firstName, eventTicket.lastName].join()}
                                                data-test-id={TestId.ticketTableRow}
                                            >
                                                <StyledTableCell component="th" scope="row">
                                                    {[eventTicket.firstName, eventTicket.lastName].join(' ')}
                                                </StyledTableCell>
                                                <StyledTableCell align="right">{eventTicket.barcode}</StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </StyledTableCell>
                </StyledTableRow>
            )}
        </>
    );
};

interface Props {
    events: Event[];
}

const EventTable = ({ events }: Props): ReactElement => (
    <TableContainer component={Paper}>
        <Table>
            <TableHead>
                <TableRow>
                    <StyledTableCell />
                    <StyledTableCell>Event</StyledTableCell>
                    <StyledTableCell align="right">City</StyledTableCell>
                    <StyledTableCell align="right">Date</StyledTableCell>
                </TableRow>
            </TableHead>
            <TableBody data-test-id={TestId.eventListTableRoot}>
                {events.map(event => (
                    <TableRowWithTickets
                        key={[event.eventTitle, event.eventCity, event.eventDate.getTime()].join()}
                        row={event}
                    />
                ))}
            </TableBody>
        </Table>
    </TableContainer>
);

export default EventTable;
