import { MongoClient } from 'mongodb';
import { Event } from 'src/types/Event';

const connectionString = process.env.MONGODB_CONNECTION_STRING ?? '';

const db = {
    isConnected: false,
    client: new MongoClient(connectionString),
};

db.client.on('open', () => db.isConnected = true);
db.client.on('topologyClosed', () => db.isConnected = false);

const getApiClient = async () => {
    if (!db.isConnected) {
        await db.client.connect();
    }

    return {
        client: db.client,
        events: db.client.db().collection<Event>(eventsCollection),
    };
};

// "events" was a reserved name
export const eventsCollection = 'eventList';

export default getApiClient;
