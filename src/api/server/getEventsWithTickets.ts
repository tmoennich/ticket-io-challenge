import getApiClient from 'src/api/server/getApiClient';
import { Event } from 'src/types/Event';

const getEventsWithTickets = async (): Promise<Event[]> => {

    const { events } = await getApiClient();

    return events.find().toArray();
};

export default getEventsWithTickets;
