import { Event } from 'src/types/Event';
import { EventTicket } from 'src/types/EventTicket';
import getApiClient from 'src/api/server/getApiClient';

const createEventWithTickets = async (event: Event, tickets: EventTicket[]): Promise<object | Error> => {

    const { events } = await getApiClient();

    try {
        return events.insertOne({
            eventTitle: event.eventTitle,
            eventCity: event.eventCity,
            eventDate: event.eventDate,
            tickets,
        });
    } catch (error) {
        return error as Error;
    }
};

export default createEventWithTickets;
