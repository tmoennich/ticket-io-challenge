import { Event } from 'src/types/Event';

export type ApiEvent = Omit<Event, 'eventDate'> & {
    eventDate: string;
}
