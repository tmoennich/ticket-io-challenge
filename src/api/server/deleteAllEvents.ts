import getApiClient from 'src/api/server/getApiClient';

const deleteAllEvents = async (): Promise<object | Error> => {

    const { events } = await getApiClient();

    try {
        return events.deleteMany({});
    } catch (error) {
        return error as Error;
    }
};

export default deleteAllEvents;
