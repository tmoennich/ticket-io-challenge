const deleteAllEvents = (): Promise<void> =>
    fetch(
        '/api/events', {
            method: 'DELETE',
        }
    ).then(res => res.json());

export default deleteAllEvents;
