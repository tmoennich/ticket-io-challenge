import { Event } from 'src/types/Event';
import { EventTicket } from 'src/types/EventTicket';

type CreateResponse = {
    acknowledged: true;
    insertedId: string;
}

const createEvent = (data: { event: Event, tickets: EventTicket[] }): Promise<CreateResponse> => fetch(
    '/api/events',
    {
        method: 'post',
        body: JSON.stringify(data),
    }
).then(res => res.json());

export default createEvent;
