import { Event } from 'src/types/Event';
import { parseISO } from 'date-fns';
import { ApiEvent } from 'src/api/server/types/ApiEvent';

const getEvents = (): Promise<Event[]> => fetch('/api/events')
    .then(async res => {
        const events = await res.json() as unknown as ApiEvent[];

        return events.map<Event>(event => ({
            ...event,
            eventDate: parseISO(event.eventDate)
        }));
    });

export default getEvents;
