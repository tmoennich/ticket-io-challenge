import { ReactElement } from 'react';
import { Add } from '@mui/icons-material';

export type NavLink = {
    title: ReactElement | string;
    path: string;
}

const navLinks: NavLink[] = [
    { title: <><Add /> Create event</>, path: '/create-event' }
];

export default navLinks;
