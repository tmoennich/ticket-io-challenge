import type { NextPage } from 'next';
import { Box, Button, Container, Paper } from '@mui/material';
import { useMutation, useQuery } from 'react-query';
import getEvents from 'src/api/client/getEvents';
import EventTable from 'src/components/event-table/EventTable';
import { Delete } from '@mui/icons-material';
import { useCallback } from 'react';
import deleteAllEvents from 'src/api/client/deleteAllEvents';


const Home: NextPage = () => {

    const { data, refetch } = useQuery('events', getEvents);
    const deleteAllEventsMutation = useMutation(deleteAllEvents);

    const handleDeleteAllEvents = useCallback(async () => {
        await deleteAllEventsMutation.mutateAsync();

        refetch();
    }, [deleteAllEventsMutation, refetch]);

    const eventList = data ?? [];

    return (
        <Container component="main" maxWidth="md">
            <h1>Event List</h1>

            {eventList.length > 0 ? (
                <>
                    <EventTable events={eventList} />

                    <Paper variant="outlined" sx={{ my: 6, p: 3 }}>
                        <Button startIcon={<Delete />} color="error" onClick={handleDeleteAllEvents}>
                            Delete all events
                        </Button>
                    </Paper>
                </>
            ) : (
                <Box>
                    <p>
                        No events defined yet.
                    </p>
                </Box>
            )}

        </Container>
    );
};

export default Home;
