import type { NextPage } from 'next';
import { Container, Paper, Typography } from '@mui/material';
import EventForm from 'src/components/event-form/EventForm';
import { useCallback } from 'react';
import { EventTicket } from 'src/types/EventTicket';
import { Event } from 'src/types/Event';
import { useMutation } from 'react-query';
import createEvent from 'src/api/client/createEvent';
import { useSnackbar } from 'notistack';

const CreateEventPage: NextPage = () => {

    const { enqueueSnackbar } = useSnackbar();
    const createEventMutation = useMutation(createEvent);

    const handleEventCreation = useCallback(async (event: Event, tickets: EventTicket[]): Promise<boolean> => {
        const result = await createEventMutation.mutateAsync({ event, tickets });
        if (result.acknowledged) {
            enqueueSnackbar('Event created!', { variant: 'success' });

            return true;
        }

        enqueueSnackbar('Event creation failed', { variant: 'error' });

        return false;
    }, [createEventMutation, enqueueSnackbar]);

    return (
        <>
            {/* A hack to overcome a vertical scrollbar produced by the confetti container */}
            <style global jsx>{`
                html,
                body {
                    overflow-x: hidden;
                }
            `}</style>

            <Container component="main" maxWidth="sm">
                <Paper variant="outlined" sx={{ my: 6, p: 3 }}>
                    <Typography component="h1" variant="h4" align="center">
                        Create a new event
                    </Typography>

                    <EventForm onCreate={handleEventCreation} />
                </Paper>
            </Container>
        </>
    );
};

export default CreateEventPage;
