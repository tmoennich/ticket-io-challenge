import type { NextApiRequest, NextApiResponse } from 'next';
import { Event } from 'src/types/Event';
import { EventTicket } from 'src/types/EventTicket';
import createEventWithTickets from 'src/api/server/createEventWithTickets';
import getEventsWithTickets from 'src/api/server/getEventsWithTickets';
import deleteAllEvents from 'src/api/server/deleteAllEvents';

type PostData = {
    event?: Event,
    tickets?: EventTicket[];
}

type CreateEventApiRequest = NextApiRequest & {
    method: 'POST',
    body: PostData;
}

type DeleteEventsApiRequest = NextApiRequest & {
    method: 'DELETE',
}

const events = async (req: CreateEventApiRequest | DeleteEventsApiRequest, res: NextApiResponse) => {
    if (req.method === 'POST') {
        const postData = JSON.parse(req.body) as PostData;
        if (postData.event && postData.tickets) {
            const response = await createEventWithTickets(postData.event, postData.tickets);

            res.status(200)
                .json(JSON.parse(JSON.stringify(response)));
            return;
        }
    }

    if (req.method === 'DELETE') {
        await deleteAllEvents();

        res.status(200)
            .json({ done: true });
        return;
    }

    const allEvents = await getEventsWithTickets();

    res.status(200)
        .json(JSON.parse(JSON.stringify(allEvents)));
};

export default events;
