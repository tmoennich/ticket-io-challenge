import React, { ReactElement } from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';
import createEmotionServer from '@emotion/server/create-instance';
import createEmotionCache from 'src/utils/createEmotionCache';
import { DocumentContext, DocumentInitialProps } from 'next/dist/shared/lib/utils';
import darkTheme from 'src/styles/darkTheme';

export default class MyDocument extends Document {

    public static async getInitialProps(context: DocumentContext): Promise<DocumentInitialProps> {

        const originalRenderPage = context.renderPage;

        // You can consider sharing the same emotion cache between all the SSR requests to speed up performance.
        // However, be aware that it can have global side effects.
        const cache = createEmotionCache();
        const { extractCriticalToChunks } = createEmotionServer(cache);

        context.renderPage = () => originalRenderPage({
            /* eslint-disable */
            enhanceApp: (App: any) => (props) => (
                <App emotionCache={cache} {...props} />
            ),
            /* eslint-enable */
        });

        const initialProps = await Document.getInitialProps(context);

        // Create the emotion to be prepared for SSR
        // See https://github.com/mui-org/material-ui/issues/26561#issuecomment-855286153
        const emotionStyles = extractCriticalToChunks(initialProps.html);
        const emotionStyleTags = emotionStyles.styles.map((style) => (
            <style
                data-emotion={`${style.key} ${style.ids.join(' ')}`}
                key={style.key}
                dangerouslySetInnerHTML={{ __html: style.css }}
            />
        ));

        return {
            ...initialProps,
            styles: [
                ...React.Children.toArray(initialProps.styles),
                ...emotionStyleTags,
            ],
        };
    }

    public render(): ReactElement {
        return (
            <Html lang="en">
                <Head>
                    <meta name="theme-color" content={darkTheme.palette.primary.main} />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
