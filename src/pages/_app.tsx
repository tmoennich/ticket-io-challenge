import React, { ReactElement } from 'react';
import type { AppProps } from 'next/app';
import { CacheProvider, EmotionCache } from '@emotion/react';
import { Box, CssBaseline, ThemeProvider } from '@mui/material';
import createEmotionCache from 'src/utils/createEmotionCache';
import PageFooter from 'src/components/footer/PageFooter';
import darkTheme from 'src/styles/darkTheme';
import PageHeader from 'src/components/header/PageHeader';
import { LocalizationProvider } from '@mui/lab';
import DateAdapter from '@mui/lab/AdapterDateFns';
import Head from 'next/head';
import { default as dateFnsLocaleGerman } from 'date-fns/locale/de';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { SnackbarProvider } from 'notistack';

const queryClient = new QueryClient();

const clientSideEmotionCache = createEmotionCache();

interface Props extends AppProps {
    emotionCache?: EmotionCache;
}

const MyApp = ({ Component, emotionCache = clientSideEmotionCache, pageProps }: Props): ReactElement => (
    <QueryClientProvider client={queryClient}>
        <ReactQueryDevtools />

        <CacheProvider value={emotionCache}>
            <LocalizationProvider
                dateAdapter={DateAdapter}
                locale={dateFnsLocaleGerman}
            >
                <ThemeProvider theme={darkTheme}>
                    <SnackbarProvider maxSnack={3}>
                        <Head>
                            <title>ticket.io challenge</title>
                        </Head>

                        <CssBaseline />

                        <PageHeader />

                        <Box>
                            <Component {...pageProps} />
                        </Box>

                        <PageFooter />
                    </SnackbarProvider>
                </ThemeProvider>
            </LocalizationProvider>
        </CacheProvider>
    </QueryClientProvider>
);

export default MyApp;
