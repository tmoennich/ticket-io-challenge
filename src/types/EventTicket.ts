export type EventTicket = {
    firstName: string;
    lastName: string;
    barcode: string;
}