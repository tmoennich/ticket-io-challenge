import { EventTicket } from 'src/types/EventTicket';

export type Event = {
    eventTitle: string;
    eventCity: string;
    eventDate: Date;
    tickets?: EventTicket[];
}
