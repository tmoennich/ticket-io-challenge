import { useState } from 'react';
import { EventTicket } from 'src/types/EventTicket';
import { useSnackbar } from 'notistack';

type EventTicketList = EventTicket[];
type AddTicketHandler = (ticket: EventTicket) => void;
type RemoveTicketHandler = (indexToDelete: number) => void;
type RemoveAllHandler = () => void;

const useTicketList = (initialTicketList: EventTicketList): [
    EventTicketList,
    AddTicketHandler,
    RemoveTicketHandler,
    RemoveAllHandler
] => {

    const [ticketList, setTicketList] = useState<EventTicketList>(initialTicketList);
    const { enqueueSnackbar } = useSnackbar();

    const addTicket: AddTicketHandler = (ticket: EventTicket) => {
        setTicketList([...ticketList, ticket]);

        enqueueSnackbar('Ticket added!', { variant: 'success' });
    };

    const removeTicket: RemoveTicketHandler = (indexToDelete: number) => {
        if (indexToDelete < 0 || indexToDelete >= ticketList.length) {
            return;
        }

        setTicketList([...ticketList.filter((_, index) => index !== indexToDelete)]);

        enqueueSnackbar('Ticket removed!', { variant: 'info' });
    };

    const removeAllTickets: RemoveAllHandler = () => setTicketList([]);

    return [ticketList, addTicket, removeTicket, removeAllTickets];
};

export default useTicketList;
