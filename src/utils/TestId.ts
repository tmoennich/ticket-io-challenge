export enum TestId {
    eventListTableRoot = 'eventListTableRoot',
    eventListTicketToggleTableCell = 'eventListTicketToggleTableCell',
    ticketTableRow = 'ticketTableRow',

    createEventForm = 'createEventForm',
    createEventFormSubmit = 'createEventFormSubmit',
    createTicketFormSubmit = 'createTicketFormSubmit',
    deleteTicketButton = 'deleteTicketButton',
}
